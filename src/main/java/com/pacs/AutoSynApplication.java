package com.pacs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling // 开启定时任务功能
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class AutoSynApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoSynApplication.class, args);
    }

}
