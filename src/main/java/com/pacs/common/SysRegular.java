package com.pacs.common;

import com.pacs.entity.SysRegularEntity;
import com.pacs.mapping.SysRegularMapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Component
public class SysRegular {
    @Resource
    private CustomerUtil customerUtil;
    @Resource
    private SysRegularMapper regularMapper;

    /** 获取系统规则增长的ID
     * @param title 要增长的字段类型
     * @return
     */
    public synchronized String getId(String title) {
        SysRegularEntity regularEntity=regularMapper.get(title);
        int len =regularEntity.getRarSeqLen();
        int value=regularEntity.getRarValue();
        customerUtil.setTotalCount(value);
        regularEntity.setRarValue(value+1);
        int result=regularMapper.update(regularEntity);
        if(result != 1) {
            System.out.println("获取自增长ID失败");
        }
        return customerUtil.getCustomerID(customerUtil.setLength(len));
    }
}
