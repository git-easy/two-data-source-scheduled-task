package com.pacs.common;

import org.springframework.stereotype.Component;

import java.text.DecimalFormat;

/**CustomerUtil
 * @author 007
 * @date 2018年8月22日
 * @Description
 */
@Component
public class CustomerUtil {

	private int customerID;
	private CustomerUtil(){

	}
	public String getCustomerID(String length) {
		DecimalFormat decimalFormat = new DecimalFormat(length);
		return decimalFormat.format(customerID);
	}
	public String setLength(int len) {
		String length = "";
		for (int i = 0; i < len; i++) {
			length+="0";
		}
		return length;
	}

	public void setTotalCount(int totalCount) {
		++totalCount;
		customerID = totalCount;
	}
}
