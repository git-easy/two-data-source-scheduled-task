package com.pacs.common;

import com.pacs.config.Properties;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;

@Component
public class FunCommon {

    public static Properties config;
    @Resource
    private void setConfig(Properties config){
        FunCommon.config = config;
    }


    /**
     * 传图检查类型转换
     * @param examClass
     * @return
     */
    public static String classTypeChange(String examClass){
        if (StringUtils.isEmpty(examClass)) return "";
        for(String value : config.getXray()){
            if(examClass.equals(value)){
                return "X-RAY";
            }
        }
        for(String value : config.getMr()){
            if(examClass.equals(value)){
                return "MR";
            }
        }
        for(String value : config.getDsa()){
            if(examClass.equals(value)){
                return "DSA";
            }
        }
        for(String value : config.getUs()){
            if(examClass.equals(value)){
                return "US";
            }
        }
        return examClass;
    }

}
