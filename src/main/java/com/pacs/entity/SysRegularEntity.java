package com.pacs.entity;

/**SystemRegular
 * @author 007
 * @Description 
 * 创建时间  2018年8月17日
 */
public class SysRegularEntity {
	/**
	 * 规则标题
	 */
	private String rarTitle;
	/**
	 * 顺序号长度
	 */
	private Integer rarSeqLen;
	/**
	 * 顺序号当前值
	 */
	private Integer rarValue;
	public String getRarTitle() {
		return rarTitle;
	}
	public void setRarTitle(String rarTitle) {
		this.rarTitle = rarTitle;
	}
	public Integer getRarSeqLen() {
		return rarSeqLen;
	}
	public void setRarSeqLen(Integer rarSeqLen) {
		this.rarSeqLen = rarSeqLen;
	}
	public Integer getRarValue() {
		return rarValue;
	}
	public void setRarValue(Integer rarValue) {
		this.rarValue = rarValue;
	}
}
