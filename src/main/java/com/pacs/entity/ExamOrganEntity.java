package com.pacs.entity;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

/**ExamOrganEntity
 * @author 007
 * @Description 
 * 创建时间  2018年8月17日
 */
public class ExamOrganEntity implements Serializable {
	private static final long serialVersionUID = 1L;
//	检查ID	EXAM_ID
	private String examId;
//	部位序号	ORGAN_NO
	private String organNo;
//	部位代码	ORGAN_CODE
	private String organCode;
//	部位名称	ORGAN_NAME
	private String organName;
	public String getExamId() {
		return examId;
	}
	public void setExamId(String examId) {
		this.examId = examId;
	}
	public String getOrganNo() {
		return organNo;
	}
	public void setOrganNo(String organNo) {
		this.organNo = organNo;
	}
	public String getOrganCode() {
		return organCode;
	}
	public void setOrganCode(String organCode) {
		this.organCode = organCode;
	}
	public String getOrganName() {
		return organName;
	}
	public void setOrganName(String organName) {
		this.organName = organName;
	}
	
}
