package com.pacs.entity;

/**第三方视图同步记录
 * @author 007
 * @date 2018年10月24日
 * @Description
 */
public class ExamSynRecEntity {
	private String applyId;
	private String synDateTime;
	private String flags;
	public String getApplyId() {
		return applyId;
	}
	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
	public String getSynDateTime() {
		return synDateTime;
	}
	public void setSynDateTime(String synDateTime) {
		this.synDateTime = synDateTime;
	}
	public String getFlags() {
		return flags;
	}
	public void setFlags(String flags) {
		this.flags = flags;
	}
	
}
