package com.pacs.entity;

import java.io.Serializable;
import java.util.List;

import static com.pacs.common.FunCommon.classTypeChange;

/**ExamEntity
 * @author 007
 * @Description 
 * 创建时间  2018年8月17日
 */
public class ExamEntity implements Serializable {
//	检查ID	EXAM_ID
	public String examId;
	//申请单号
	public String applyId;
//	送检单位编号	EXAM_ NO
	public String examNo;
//	患者ID	PAT_ID
	public String patId;
//	患者编号	PAT_NO
	public String patNo;
	public String patName;
//	检查类型	EXAM_CLASS
	public String examClass;
//	病人来源	PAT_SOURCE
	public String patSource;
	// 检查间
	public String roomId;
//	门诊号	PAT_OUT_NO
	public String patOutNo;
//	住院号	PAT_IN_NO
	public String  patInNo;
	//体检号
	public String patPhyNo;
	//借检号
	public String patLendNo;

//	检查级别	EXAM_LEVEL
	public String examLevel;
//	影像年龄	PAT_AGE
	public String patAge;
//	影像性别	PAT_SEX
	public String patSex;
//  检查部位	
	public List<ExamOrganEntity> organs;
//	检查日期	EXAM_DT
	public String examDt;
//	身高	PAT_HEIGHT
	public String patHeight;
//	体重	PAT_WEIGHT
	public String patWeight;
//	床号	BED_NO
	public String bedNo;
//	通信地址	PAT_ADDRESS
	public String patAddress;
//	联系电话	PAT_PHONE
	public String patPhone;
//	申请科室名称	CLIN_DEPT
	public String clinDept;
//	申请医生	CLIN_DOC
	public String clinDoc;
//	临床症状	CLIN_SYMP
	public String clinSymp;
//	临床诊断	CLIN_DIAG
	public String clinDiag;
//	检查目的	EXAM_NOTICE
	public String examNotice;
//	申请备注	EXAM_MEMO
	public  String examMemo;
//	申请日期	REQ_DT
	public String reqDt;
//	检查次数	EXAM_COUNT
	public Integer examCount;
//  检查颜色
	public String examColor;
//	检查费用	EXAM_PRICE
	public String examPrice;
//	标志	EXAM_STATUS
	public String examStatus;
//	危机值	EXAM_RISKS
	public String examRisks;
//	锁定状态	LOCKED_FLAGS
	public String lockedFlags;
//	锁定者ID	LOCKED_USER_ID
	public String lockedUserId;
//	锁定者用户名	LOCKED_USER_NAME
	public String lockedUserName;
//	锁定时间	LOCKED_DT
	public String lockedDt;
//	执行科室ID 	EXECUTE_DEPT_ID
	public String executeDeptId;
	public String executeDeptCode;
//	执行科室名称 	EXECUTE_DEPT_NAME
	public String executeDeptName;
//	执行医生ID 	EXECUTE_USER_ID
	public String executeUserId;
//	执行医生姓名 	EXECUTE_USER_NAME
	public String executeUserName;
//	放射云ID	FSY_ID
	public String fsyId;
//	是否传输上云	FSY_FLAGS
	public String fsyFlags;
//	传输上云的状态	FSY_STATUS
	public String fsyStatus;
	//HIS登记标志(默认0)
	public String hisStatus = "0";
	
	public ExamPatientEntity patient;

	public List<ExamItemsEntity> items;
 	
	public String getFsyFlags() {
		return fsyFlags;
	}
	public void setFsyFlags(String fsyFlags) {
		this.fsyFlags = fsyFlags;
	}
	public String getFsyStatus() {
		return fsyStatus;
	}
	public void setFsyStatus(String fsyStatus) {
		this.fsyStatus = fsyStatus;
	}
	public String getExamId() {
		return examId;
	}
	public void setExamId(String examId) {
		this.examId = examId;
	}
	public String getApplyId() {
		return applyId;
	}
	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}
	public String getExamNo() {
		return examNo;
	}
	public void setExamNo(String examNo) {
		this.examNo = examNo;
	}
	public String getPatId() {
		return patId;
	}
	public void setPatId(String patId) {
		this.patId = patId;
	}
	public String getPatNo() {
		return patNo;
	}
	public void setPatNo(String patNo) {
		this.patNo = patNo;
	}
	public void setPatName(String patName) {
		this.patName = patName;
	}
	public String getPatName() {
		return patName;
	}
	public List<ExamOrganEntity> getOrgans() {
		return organs;
	}
	public void setOrgans(List<ExamOrganEntity> organs) {
		this.organs = organs;
	}
	public String getExamClass() {
		return examClass;
	}
	public void setExamClass(String examClass) {
		this.examClass = classTypeChange(examClass);
	}
	public String getPatSource() {
		return patSource;
	}
	public void setPatSource(String patSource) {
		this.patSource = patSource;
	}
	public String getPatOutNo() {
		return patOutNo;
	}
	public void setPatOutNo(String patOutNo) {
		this.patOutNo = patOutNo;
	}
	public String getPatInNo() {
		return patInNo;
	}
	public void setPatInNo(String patInNo) {
		this.patInNo = patInNo;
	}
	
	public String getPatPhyNo() {
		return patPhyNo;
	}
	public void setPatPhyNo(String patPhyNo) {
		this.patPhyNo = patPhyNo;
	}
	public String getPatLendNo() {
		return patLendNo;
	}
	public void setPatLendNo(String patLendNo) {
		this.patLendNo = patLendNo;
	}
	public String getRoomId() {
		return roomId;
	}
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}
	public String getExamLevel() {
		return examLevel;
	}
	public void setExamLevel(String examLevel) {
		this.examLevel = examLevel;
	}
	public String getPatAge() {
		return patAge;
	}
	public void setPatAge(String patAge) {
		this.patAge = patAge;
	}
	public String getPatSex() {
		return patSex;
	}
	public void setPatSex(String patSex) {
		this.patSex = patSex;
	}
	public String getExamDt() {
		if(examDt==null||examDt.equals("")||examDt.length()<19) {
			return examDt;
		}
		return examDt.substring(0, 19);
	}
	public void setExamDt(String examDt) {
		this.examDt = examDt;
	}
	public String getPatHeight() {
		return patHeight;
	}
	public void setPatHeight(String patHeight) {
		this.patHeight = patHeight;
	}
	public String getPatWeight() {
		return patWeight;
	}
	public void setPatWeight(String patWeight) {
		this.patWeight = patWeight;
	}
	public String getBedNo() {
		return bedNo;
	}
	public void setBedNo(String bedNo) {
		this.bedNo = bedNo;
	}
	public String getPatAddress() {
		return patAddress;
	}
	public void setPatAddress(String patAddress) {
		this.patAddress = patAddress;
	}
	public String getPatPhone() {
		return patPhone;
	}
	public void setPatPhone(String patPhone) {
		this.patPhone = patPhone;
	}
	public String getClinDept() {
		return clinDept;
	}
	public void setClinDept(String clinDept) {
		this.clinDept = clinDept;
	}
	public String getClinDoc() {
		return clinDoc;
	}
	public void setClinDoc(String clinDoc) {
		this.clinDoc = clinDoc;
	}
	public String getClinSymp() {
		return clinSymp;
	}
	public void setClinSymp(String clinSymp) {
		this.clinSymp = clinSymp;
	}
	public String getClinDiag() {
		return clinDiag;
	}
	public void setClinDiag(String clinDiag) {
		this.clinDiag = clinDiag;
	}
	public String getExamNotice() {
		return examNotice;
	}
	public void setExamNotice(String examNotice) {
		this.examNotice = examNotice;
	}
	public String getExamMemo() {
		return examMemo;
	}
	public void setExamMemo(String examMemo) {
		this.examMemo = examMemo;
	}
	public String getReqDt() {
		if(reqDt==null||reqDt.equals("")||reqDt.length()<19) {
			return reqDt;
		}
		return reqDt.substring(0, 19);
	}

	public void setReqDt(String reqDt) {
		this.reqDt = reqDt;
	}
	public String getExamColor() {
		return examColor;
	}
	public void setExamColor(String examColor) {
		this.examColor = examColor;
	}
	public Integer getExamCount() {
		return examCount;
	}
	public void setExamCount(Integer examCount) {
		this.examCount = examCount;
	}
	public String getExamPrice() {
		return examPrice;
	}
	public void setExamPrice(String examPrice) {
		this.examPrice = examPrice;
	}
	public String getExamStatus() {
		return examStatus;
	}
	public void setExamStatus(String examStatus) {
		this.examStatus = examStatus;
	}
	public String getExamRisks() {
		return examRisks;
	}
	public void setExamRisks(String examRisks) {
		this.examRisks = examRisks;
	}
	public String getExecuteDeptId() {
		return executeDeptId;
	}
	public void setExecuteDeptId(String executeDeptId) {
		this.executeDeptId = executeDeptId;
	}
	public String getExecuteDeptCode() {
		return executeDeptCode;
	}
	public void setExecuteDeptCode(String executeDeptCode) {
		this.executeDeptCode = executeDeptCode;
	}
	public String getExecuteDeptName() {
		return executeDeptName;
	}
	public void setExecuteDeptName(String executeDeptName) {
		this.executeDeptName = executeDeptName;
	}
	public String getExecuteUserId() {
		return executeUserId;
	}
	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}
	public String getExecuteUserName() {
		return executeUserName;
	}
	public void setExecuteUserName(String executeUserName) {
		this.executeUserName = executeUserName;
	}
	public String getLockedFlags() {
		return lockedFlags;
	}
	public void setLockedFlags(String lockedFlags) {
		this.lockedFlags = lockedFlags;
	}
	public String getLockedUserId() {
		return lockedUserId;
	}
	public void setLockedUserId(String lockedUserId) {
		this.lockedUserId = lockedUserId;
	}
	public String getLockedUserName() {
		return lockedUserName;
	}
	public void setLockedUserName(String lockedUserName) {
		this.lockedUserName = lockedUserName;
	}
	public String getLockedDt() {
		return lockedDt;
	}
	public void setLockedDt(String lockedDt) {
		this.lockedDt = lockedDt;
	}
	public String getFsyId() {
		return fsyId;
	}
	public void setFsyId(String fsyId) {
		this.fsyId = fsyId;
	}

	public String getHisStatus(){
		return hisStatus;
	}
	public void setHisStatus(String hisStatus) {
		this.hisStatus = hisStatus;
	}

	public ExamPatientEntity getPatient() {
		return patient;
	}

	public void setPatient(ExamPatientEntity patient) {
		this.patient = patient;
	}
}
