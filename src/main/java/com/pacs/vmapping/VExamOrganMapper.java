package com.pacs.vmapping;

import com.pacs.entity.ExamOrganEntity;

import java.util.List;

public interface VExamOrganMapper {

    List<ExamOrganEntity> select(String applyId);
}
