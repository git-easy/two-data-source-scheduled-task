package com.pacs.vmapping;

import com.pacs.entity.ExamItemsEntity;

import java.util.List;

public interface VExamItemMapper {

    List<ExamItemsEntity> select(String applyId);
}
