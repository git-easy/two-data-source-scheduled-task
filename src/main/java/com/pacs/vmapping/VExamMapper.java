package com.pacs.vmapping;

import com.pacs.entity.ExamEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VExamMapper {
    /**
     * 通过applyId查询XX天
     * @param days
     */
    List<ExamEntity> selectByIntervalDays(@Param("days") int days, @Param("patSource") String patSource);

}
