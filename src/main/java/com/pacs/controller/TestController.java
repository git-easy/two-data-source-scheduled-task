package com.pacs.controller;

import com.pacs.vmapping.VExamItemMapper;
import com.pacs.vmapping.VExamMapper;
import com.pacs.vmapping.VExamOrganMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class TestController {
    @Resource
    private VExamMapper vExamMapper;
    @Resource
    private VExamItemMapper vExamItemMapper;
    @Resource
    private VExamOrganMapper vExamOrganMapper;

    @RequestMapping("/test")
    public String test() {
        return "Tomcat 运行正常";
    }
}
