package com.pacs.mapping;

import com.pacs.entity.ExamItemsEntity;

import java.util.List;

public interface ExamItemMapper {
    int insert(List<ExamItemsEntity> items);
}
