package com.pacs.mapping;

import com.pacs.entity.ExamPatientEntity;

public interface ExamPatientMapper {
    int insert(ExamPatientEntity patient);
}
