package com.pacs.mapping;

import com.pacs.entity.SysRegularEntity;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface SysRegularMapper {
    @Select("SELECT * FROM SYSTEM_REGULAR WHERE RAR_TITLE=#{value}")
    SysRegularEntity get(String title);

    @Update("UPDATE SYSTEM_REGULAR " +
            "set " +
            "RAR_SEQ_LEN=#{rarSeqLen}," +
            "RAR_VALUE=#{rarValue} " +
            "WHERE RAR_TITLE=#{rarTitle}")
    int update(SysRegularEntity regularEntity);
}
