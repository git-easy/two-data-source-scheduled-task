package com.pacs.mapping;

import com.pacs.entity.ExamSynRecEntity;

import java.util.List;

public interface ExamSynRecMapper {
    int insert(ExamSynRecEntity examSynRecEntity);
}
