package com.pacs.mapping;

import com.pacs.entity.ExamEntity;

public interface ExamMapper {
    int insert(ExamEntity exam);
}
