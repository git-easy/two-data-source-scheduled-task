package com.pacs.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource(value = "classpath:config/fsyPACS.properties", encoding="UTF-8",ignoreResourceNotFound = true)
@ConfigurationProperties(prefix = "")
public class Properties {
    private int intervalDays;
    private String patSource;
    private int synFlag;
    private String[] xray;

    private String[] mr;

    private String[] dsa;

    private String[] us;

    public int getIntervalDays() {
        return intervalDays;
    }

    public void setIntervalDays(int intervalDays) {
        this.intervalDays = intervalDays;
    }

    public String getPatSource() {
        return patSource;
    }

    public void setPatSource(String patSource) {
        this.patSource = patSource;
    }

    public int getSynFlag() {
        return synFlag;
    }

    public void setSynFlag(int synFlag) {
        this.synFlag = synFlag;
    }

    public String[] getXray() {
        return xray;
    }

    public void setXray(String[] xRay) {
        this.xray = xRay;
    }

    public String[] getMr() {
        return mr;
    }

    public void setMr(String[] mr) {
        this.mr = mr;
    }

    public String[] getDsa() {
        return dsa;
    }

    public void setDsa(String[] dsa) {
        this.dsa = dsa;
    }

    public String[] getUs() {
        return us;
    }

    public void setUs(String[] us) {
        this.us = us;
    }
}
